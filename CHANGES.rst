Changelog
=========


0.3.1 (unreleased)
------------------

- Nothing changed yet.


0.3 (2014-11-28)
----------------

- Added scan_id as index [sgeulette]


0.2.1 (2014-09-02)
------------------

- Updated changes.
- Corrected manifest.

0.2 (2014-04-15)
----------------

- renamed IScan in IScanFields.
- changed zcml description of behavior
- translate the title and description of the behavior in the interface.
- delete the folder content.
- moved the zcml definition of import step in profile.
- correct the manifest.
- renamed "scan_user" to "operator".

0.1 (2014-04-14)
----------------

- Initial release.
  [fngaha]

